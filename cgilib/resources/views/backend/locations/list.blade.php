
@extends('layouts.backend')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $table_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ $table_name }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
 
        

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
	                 <a href="{{ url('/location-create') }}" class="btn btn-primary btn-lg btn-flat">
	                  <i class="fas fa-cart-plus fa-xs mr-2"></i> 
	                  Create
	                </a>          	
                </h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>SL#</th>
                      <th>Name</th>
                      <th>Status</th>                  
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($locations as $l)
                    <tr>
                      <td>{{$l->id}}</td>
                      <td>{{$l->name}}</td>
                      <td>
                        @if($l->is_active)
                        <span class="badge badge-success">Active</span>
                        @else 
                        <span class="badge badge-danger">InActive</span>
                        @endif
                      </td>

                       <td>                        
                        <a href="{{ url('/categories/edit/'. $l->id) }}" class="btn
                          btn-info">
                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        <button class="btn btn-danger" type="button" onclick="deleteItem({{ $l->id }})">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                        <form id="delete-form-{{ $l->id }}" action="{{ route('categories', $l->id) }}" method="post" style="display:none;">
                          {{csrf_field()}}
                          <input type="hidden" name="id" value="{{$l->id}}" />
                        </form>
                      </td>     
                      
                    </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
                <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>	


            </div>
            <!-- /.card -->
          </div>
        </div>


        


      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection

@section('custom_js')
<!-- Sweet Alert Js -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.29.1/dist/sweetalert2.all.min.js"></script>

 <script type="text/javascript">
        function deleteItem(id) {
            const swalWithBootstrapButtons = swal.mixin({
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
            })

            swalWithBootstrapButtons({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>


@endsection