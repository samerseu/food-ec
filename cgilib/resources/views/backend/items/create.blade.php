@extends('layouts.backend')
@section('content')
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $table_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ $table_name }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                   <a href="{{ url('/item-list') }}" class="btn btn-primary btn-lg btn-flat">
                    <i class="fas fa-cart-plus fa-xs mr-2"></i> 
                   Return to list
                  </a>   

                </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data" action="{{ route('items-create') }}">
                {{ csrf_field() }}
                <div class="card-body">

                 
                 

                  <div class="form-group">
                    <label for="category_id">Category</label>
                    <select class="select2 form-control"id="category_id" name="category_id" >
                      @foreach($categories as $c)
                        <option value="{{ $c->id }}">{{ $c->name }}</option>
                      @endforeach  
                    </select>
                  </div>


                  <div class="form-group">
                    <label for="shop_id">Shop</label>
                    <select class="select2 form-control" id="shop_id" name="shop_id" >
                      @foreach($shops as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                      @endforeach  
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Item Name">
                  </div>

                    <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="address" name="description" placeholder="Description">
                  </div>

                  <div class="form-group">
                    <label for="price">Price</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                  </div>     


                  <div class="form-group">
                    <label for="tag">Tag</label>
                    <input type="text" class="form-control" id="tag" name="tag" placeholder="Tag">
                  </div>     


                 


                  <div class="form-group">
                    <label for="item_image">Image</label>
                    <input type="file" class="form-control" id="item_image" name="item_image[]" >
                    <button type="button" id="item_more_image_btn" class="btn btn-primary btn-xs">Add More</button>
                  </div>     

                  <div class="form-group" id="more_item_images">
                  </div>     

 

                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </form>

              @if($errors->any())
                <ul>
                    @foreach($errors->all() as $err)
                    <tr>
                        <td>
                            <li>{{$err}}</li>
                        </td>
                    </tr>
                    @endforeach
                </ul>
                @endif
            </div>
            <!-- /.card -->

           

          </div>
          <!--/.col (left) -->
      
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('custom_js')
<script type="text/javascript">
  $("#item_more_image_btn").click(() => {
      console.log('clicked me');
      $("#more_item_images").append('<input type="file" class="form-control" id="item_image" name="item_image[]">');
  });
</script>
@endsection