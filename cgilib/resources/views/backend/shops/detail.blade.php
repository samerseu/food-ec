@extends('layouts.backend')
@section('content')
  
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$table_name}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">{{$table_name}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="../../food-ec/cgilib/public/shop/{{$shop->logo}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$shop->name}}</h3>

                <!-- <p class="text-muted text-center">Software Engineer</p> -->

               <!--  <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>
 -->
                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!--<strong><i class="fas fa-book mr-1"></i> Education</strong>

                 <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>

                <hr> -->

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                <p class="text-muted">{{$shop->address}}</p>
                <hr>
                <strong><i class="fas fa-phone mr-1"></i> Mobile</strong>
                <p class="text-muted">{{$shop->mobile}}</p> 

                <hr>
                <strong><i class="fas fa-website mr-1"></i> Website</strong>
                <p class="text-muted"> 
                  @if(!empty($shop->website))
                  <a target="_blank" href="{{ $shop->website }}">{{$shop->website}}</a>
                  @endif
                </p> 

                <hr/>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Open-Close Time</strong>
                <p>{{$shop->open_close_time}}</p>
               <!--  <p class="text-muted">
                  <span class="tag tag-danger">UI Design</span>
                  <span class="tag tag-success">Coding</span>
                  <span class="tag tag-info">Javascript</span>
                  <span class="tag tag-warning">PHP</span>
                  <span class="tag tag-primary">Node.js</span>
                </p> -->

              <!--   <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong> -->

                <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#items" data-toggle="tab">Items <span class="right badge badge-warning">{{$total_item}}</span></a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="items">
                    
                    @if($total_item > 0)
                    @foreach($items as $item)  
                    <div class="post">
                      <div class="user-block">
                        @foreach($itemImages as $img)
                        @if($img->item_id == $item->id)
                        <img class="img-fluid" src="{{ asset('/cgilib/public/item/'. $img->image) }}" alt="{{$item->name}}">
                        @endif
                        @endforeach

                        <span class="username">
                          <a href="javascript:void(0)">{{$item->name}}</a>
                         
                        </span>
                        <span class="description">{{$item->price}} BDT</span>
                        <span class="description">{{$item->category_name}}</span>
                      </div>
                    </div>
                    @endforeach
                    @endif
                  
 
                  </div>
                  <!-- /.tab-pane -->
         

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" method="post" enctype="multipart/formdata">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$shop->name}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email"  value="{{$shop->email}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Website</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" placeholder="Website" name="website" value="{{$shop->website}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Details</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="message" id="inputExperience" placeholder="Details">{{$shop->message}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Banner</label>
                        <div class="col-sm-10">
                          <img class="img-fluid" src="{{ asset('/cgilib/public/shop/'. $shop->banner ) }}" />
                          <input type="file" name="banner" class="form-control" id="banner" name="banner" >
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Logo</label>
                        <div class="col-sm-10">
                          <img class="img-fluid" src="{{ asset('/cgilib/public/shop/'. $shop->logo ) }}" />
                          <input type="file" name="logo" class="form-control" id="logo"  name="logo">
                        </div>
                      </div>
                    
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection