@extends('layouts.backend')
@section('content')
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $table_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{ $table_name }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                   <a href="{{ url('/delivery-mans') }}" class="btn btn-primary btn-lg btn-flat">
                    <i class="fas fa-cart-plus fa-xs mr-2"></i> 
                   Return to list
                  </a>   

                </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data" action="{{ route('create-delivery-mans') }}">
                {{ csrf_field() }}
                <div class="card-body">

                   
                 
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name">
                  </div>


                  <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                  </div>


                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile">
                  </div>  


                  <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                  </div>
        


                  <div class="form-group">
                    <label for="nid">NID</label>
                    <input type="text" class="form-control" id="nid" name="nid" placeholder="NID">
                  </div>     


                  <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" >
                  </div>     


                  <div class="form-group">
                    <label for="details">Details</label>
                    <textarea class="form-control" id="details" name="details"></textarea>
                  </div>     

 
                  <div class="form-group">
                    <label for="commission">Commission</label>
                    <input type="text" class="form-control" id="commission" name="commission" placeholder="Commission">
                  </div>

                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </form>
              @if($errors->any())
                <ul>
                    @foreach($errors->all() as $err)
                    <tr>
                        <td>
                            <li>{{$err}}</li>
                        </td>
                    </tr>
                    @endforeach
                </ul>
                @endif
            </div>
            <!-- /.card -->

           

          </div>
          <!--/.col (left) -->
      
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection