<?php

Route::get('/info', function () { echo phpinfo(); });
Auth::routes();


Route::get('/', 'Site\HomeController@home')->name('home');

Route::get('/food-details/{id?}', 'Site\HomeController@foodDetail');
Route::get('/restaurant-details/{id?}', 'Site\HomeController@FestaurantDetail');
Route::get('/restaurant-detail/{id}', 'Site\HomeController@restuarantDetail');


Route::get('/admin', function(){ return redirect('/dashboard'); });

Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
Route::get('/sample-form', 'Admin\DashboardController@sampleForm')->name('sample-form');


Route::get('/categories', 'Admin\CategoryController@list')->name('categories');
Route::get('/category-create', 'Admin\CategoryController@create')->name('category-create');
Route::post('/category-create', 'Admin\CategoryController@createAction')->name('category-create');

Route::get('/categories/delete/{id}', 'Admin\CategoryController@delete')->name('categories.delete');
Route::post('/categories/delete/{id}', 'Admin\CategoryController@destroy');


Route::get('/locations', 'Admin\LocationsController@index')->name('locations');
Route::get('/location-create', 'Admin\LocationsController@create')->name('location-create');
Route::post('/location-create', 'Admin\LocationsController@createAction')->name('location-create');


Route::get('/shop-list', 'Admin\ShopController@index')->name('shop-list');
Route::get('/shops-create', 'Admin\ShopController@create')->name('shops-create');
Route::post('/shops-create', 'Admin\ShopController@createAction')->name('shops-create');

Route::get('/shop-detail/{id}', 'Admin\ShopController@shopDetail');
Route::post('/shop-detail/{id}', 'Admin\ShopController@detailAction');


Route::get('/shop-delete/{id}', 'Admin\ShopController@delete')->name('shop.delete');
Route::post('/shop-delete/{id}', 'Admin\ShopController@destroy')->name('shop.delete');


Route::get('/item-list', 'Admin\ItemController@index')->name('item-list');
Route::get('/items-create', 'Admin\ItemController@create')->name('items-create');
Route::post('/items-create', 'Admin\ItemController@createAction')->name('itemszcreate');


Route::get('/items-delete/{id}', 'Admin\ItemController@delete')->name('items.delete');
Route::post('/items-delete/{id}', 'Admin\ItemController@destroy')->name('items.delete');


Route::get('/voucher-list', 'Admin\VoucherController@index')->name('voucher-list');
Route::post('/generate-voucher-list', 'Admin\VoucherController@generateVoucher')->name('generate-voucher-list');


Route::get('/delivery-mans', 'Admin\DeliveryManController@index')->name('delivery-mans');
Route::get('/delivery-mans/detail/{id}', 'Admin\DeliveryManController@detail');

Route::get('/delivery-mans/delete/{id}', 'Admin\DeliveryManController@delete')->name('delivery-mans.delete');
Route::post('/delivery-mans/delete/{id}', 'Admin\DeliveryManController@destroy')->name('delivery-mans.delete');


Route::get('/create-delivery-mans', 'Admin\DeliveryManController@create')->name('create-delivery-mans');
Route::post('/create-delivery-mans', 'Admin\DeliveryManController@createAction')->name('create-delivery-mans');


Route::get('/restaurants', 'Admin\RestaurantContoller@index')->name('restaurants');
Route::get('/restaurant-create', 'Admin\RestaurantContoller@create')->name('restaurant-create');
Route::get('/restaurant-create', 'Admin\RestaurantContoller@create')->name('reataurant-create');



