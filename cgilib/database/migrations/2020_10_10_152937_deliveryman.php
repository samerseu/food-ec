<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deliveryman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('delivery_man', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_code');
            $table->string('address');
            $table->string('nid');
            $table->string('photo');
            $table->string('details');
            $table->integer('salary');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('delivery_man');
    }
}
