<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_code');
            $table->integer('customer_id');
            $table->float('price')
            ->default(0);
            $table->string('voucher_code')
            ->default(null);
            $table->float('discount')
            ->default(0);
            $table->float('after_discount_price')
            ->default(0);
            $table->integer('delivery_man_id')->default(0);
            $table->integer('order_status')->default(0)
            ->comment('0=Processing,1=Delivered,3=Cancel');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
