<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('mobile');
            $table->string('email')
            ->default(null);
            $table->string('website')
            ->default(null);
            $table->string('logo')
            ->default(null);
            $table->string('banner')
            ->default(null);           
            
            $table->string('message')
            ->default(null);
            $table->integer('localtion_id')
            ->default(0);
            $table->integer('is_active')
            ->default(0)
            ->comment('0=Processing,1=Publish,3=Hold');
            $table->string('tag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
