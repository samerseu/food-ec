<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoucherAssigned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('voucher_assigned', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('voucher_code');                        
            $table->integer('shop_id')
            ->default(0);
            $table->string('min_price')
            ->default(0);            
            $table->string('note')
            ->default(null);
            $table->integer('is_active')
            ->default(0)
            ->comment('0=Active,1=InActive');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_assigned');
    }
}
