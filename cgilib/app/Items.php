<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = "items";
   protected $fillable = [
   	'category_id',
   	'shop_id',
   	'name', 
   	'description',
   	'price',
   	'is_active',
   	'tag'
   ];
}
