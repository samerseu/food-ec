<?php

namespace app\Lib;


class HelperLib {

	public function getRandomString($length) {
		$chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charsLength = strlen($chars);
	    $result = '';
	    for ($i = 0; $i < $length; $i++) {
	        $result .= $chars[rand(0, $charsLength - 1)];
	    }
	    return $result;
	}
}