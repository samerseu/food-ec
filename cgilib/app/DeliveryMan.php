<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryMan extends Model
{
     protected $table = "delivery_man";
   protected $fillable = [
   	'id',
   	'emp_code',
   	'name',
      'email',
   	'mobile',
   	'address',
   	'nid',
   	'photo',
   	'details',
   	'comission',
   	'created_at',
   	'updated_at'
   ];
}
