<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Restaurant;

class RestaurantContoller extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $restaurants = Restaurant::all();
        return view("backend.restaurants.restaurant-list")
        ->with('table_name', 'Restaurants')
        ->with('restaurants', $restaurants);
    }


    public function create() {
        
        return view("backend.restaurants.restaurant-create")
        ->with('table_name', 'Restaurants');
    }
}
