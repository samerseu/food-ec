<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\DeliveryMan;
use Validator;


class DeliveryManController extends Controller
{

	public function index() {
		$data['deliverymans'] = DeliveryMan::get();
		$data['table_name'] = 'Delivery Mans';
		// dd($data);
		return view("backend.deliveryman.list", $data);
	}


	public function create() {
		$data['table_name'] = 'Add Delivery Man';		
		return view("backend.deliveryman.create", $data);
	}


	public function createAction(Request $r) {
		    $validator = Validator::make($r->all() ,[
          'name' => 'required|unique:delivery_man',
          'email' => 'required|unique:delivery_man',
          'address' => 'required',          
          'mobile' => 'required|min:11|max:11|regex:/(01)[0-9]{9}/',
          'nid' => 'required|integer',    
          'photo' => 'required|mimes:jpeg,jpg,png'                  
        ]);

          // dd($r->all());

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($r->all());
        }

        $file = $r->photo;
		    $fileExt = $file->getClientOriginalExtension();
        $filename = time()."_DELIVERY_MAN_".date('Y-m-d_H_i_s'). ".". 
        $fileExt;
        $file->move(public_path('delivery-man/'), $filename); 

        $date = date('Y-m-d H:i:s');
        $d = new DeliveryMan();
        $d->emp_code  = 'EMP-'. mt_rand(111111,9999999);
        $d->name = $r->name;
        $d->email = $r->email;
        $d->mobile = $r->mobile;
        $d->address = $r->address;
        $d->nid = $r->nid;
        $d->photo = $filename;        
        $d->details = $r->details;
        $d->comission = $r->commission;
        $d->created_at = $date;
        $d->updated_at = $date;
        $d->save();
        // dd($filename, $r->all());
        Toastr::success('DeliveryMan created successfully :)','Success');
        return redirect()->back()->with('success', 'Data saved successfully');
	}


  public function detail($id) {
    
    $det = DeliveryMan::where('id', $id)->first();
    $data['table_name'] = 'Delivery Man detail';
    $data['det'] = $det;
    $data['total_new_order'] = 0;
    // dd($data);
    return view('backend.deliveryman.detail', $data);
    
  }


  public function detailAction(Request $r, $id) {
    // dd($r->all(), $id);
    return redirect()->back()->with('success', 'Data saved successfully');
  }
   
  public function delete() {
    return view('backend.deliveryman.delete');
  }

  public function destroy(Request $r) {
    $id = $r->id;

    if(!empty($id)) {
      $d = DeliveryMan::find($id);
      $d->delete();
      Toastr::success('DeliveryMan data deleted successfully :)','Success');
    }
    return redirect()->back();
  }





}
