<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use App\Shops;
use App\Locations;
use App\Items;
use App\ItemImages;
use Validator;
use DB;

class ShopController extends Controller
{
    public function index() {
    	$shops = Shops::
    select(['shops.id', 'shops.name', 'shops.mobile', 'shops.address', 'shops.is_active', 'locations.name as location'])
      ->leftJoin('locations', 'locations.id' , '=', 'shops.location_id')
    	->get();
       // dd($shops);
       return view('backend.shops.list')
       ->with('shops', $shops)
       ->with('table_name', 'Shops');     
    
    }




    public function shopDetail($id) {

      $shop = Shops::where('id', $id)->first();

      $itemDet =  Items::select([
        'items.*', DB::raw('category.name as category_name')
      ])
      ->leftjoin('category', 'category.id', '=', 'items.category_id')
      ->where('shop_id', $id);

      $total_item = $itemDet->count();
      $items = $itemDet->get();
      $itemImages = ItemImages::select([
        'item_images.id',
        DB::raw('items.id as item_id'),
        'item_images.image'
      ])
      ->leftjoin('items', 'items.id', '=', 'item_images.item_id')
      ->where('items.shop_id', $id)->get(); 

      // dd($itemImages);
      return view('backend.shops.detail')
      ->with('shop', $shop)
      ->with('items', $items)
      ->with('itemImages', $itemImages)
      ->with('total_item', $total_item)
      ->with('table_name', "Shop Detail");
    }



    public function create() {

    	$locations = Locations::get();
    	return view('backend.shops.create')
    	->with('locations', $locations)
    	->with('table_name', 'Shop Create');
    }


    public function createAction(Request $r) {
        $validator = Validator::make($r->all() ,[
          'name' => 'required|unique:shops',
          'address' => 'required',          
          'mobile' => 'required|min:11|max:11|regex:/(01)[0-9]{9}/',
          'email' => 'required|email',          
          // 'website' => 'required|unique:category'          
          'logo' => 'required|image|mimes:jpeg,jpg,png',        
          'banner' => 'required|image|mimes:jpeg,png,jpg'
        ]);

          // dd($r->all());

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($r->all());
        }


        $logoFileExt = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
        $logoFile = time()."_SHOP_LOGO_".date('Y-m-d_H_i_s'). "." . $logoFileExt;
        $logo = "";
        if (move_uploaded_file($_FILES['logo']['tmp_name'], 
          public_path("shop/". $logoFile) )) {
          $logo = $logoFile;
        }

        $bannerFileExt = pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION);
        $bannerFile = time()."_SHOP_BANNER_".date('Y-m-d_H_i_s'). "." . $bannerFileExt;
        $banner = "";
        if (move_uploaded_file($_FILES['banner']['tmp_name'], public_path("shop/". $bannerFile))) {
          $banner = $bannerFile;
        }


        $shop = new Shops();
        $shop->name = $r->name;
        $shop->address = $r->address;
        $shop->mobile = $r->mobile;
        $shop->email = $r->email;
        $shop->website = $r->website;
        $shop->open_close_time = $r->open_close_time;
        $shop->logo = $logo;
        $shop->banner = $banner;
        $shop->message = null;
        $shop->location_id = implode(",", $r->location_id);
        $shop->is_active = 1;
        $shop->created_at = date('Y-m-d H:i:s');
        $shop->updated_at = date('Y-m-d H:i:s');
        $shop->save();

        Toastr::success('Shop created successfully :)','Success');

      return redirect()->back()
      ->with('success', 'Data Saved successfully');
    }



    public function delete() {
      return view('backend.shops.delete');
    }

    public function destroy(Request $r) {
      $id = $r->id;
      if(!empty($id)) {
         $s = Shops::find($id);
         $s->delete();
         Toastr::success('Shop deleted successfully :)','Success');
      }
      return redirect()->back();
    }

}
