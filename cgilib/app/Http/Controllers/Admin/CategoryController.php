<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

use App\Category;
use Validator;

class CategoryController extends Controller
{
    

	public function __construct()
    {
        $this->middleware('auth');
    }


	public function list() {
		$categories = Category::orderBy('id', 'desc')->get();
		return view('backend.category.category-list')
		->with('table_name' , 'Categories')
		->with('categories' , $categories);
	}


	public function create() {
		return view('backend.category.cetegory-create')
		->with('table_name' , 'Category Create');
	}

	public function createAction(Request $r) {
        $validator = Validator::make($r->all() ,[
          'name' => 'required|unique:category'          
        ]);

      	if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($r->all());
        }


       // dd($r->all());


		$c = new Category();
		$c->name = $r->name;
		$c->is_active = true;
		$c->created_at = date('Y-m-d H:i:s');
		$c->save();

		Toastr::success('Category created successfully :)','Success');
		return back()->with('success', 'Category added!');
	}

	public function delete($id) {
		$cat = Category::find($id);
		return view('backend.category.delete')->with('cat', $cat);
	}

	public function destroy(Request $r) {
		$c = Category::find($r->id);
		if ($c) {
			$c->delete();
		}
		Toastr::success('Category deleted successfully :)','Success');
		return back();
	}


}
