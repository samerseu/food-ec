<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Voucher;
use App\Lib\HelperLib;


class VoucherController extends Controller
{
    public function index() {
    	$data['table_name'] = 'Available Voucher List';
    	$data['vouchers'] = Voucher::where('is_used', 0)->get();
    	return view('backend.voucher.list', $data);
    }

    public function generateVoucher(Request $r) {
    	$num = 3;
    	for($i =1; $i<= $num; $i++) {
    		$h = new HelperLib();
    		$v = new Voucher();	
    		$v->code = $h->getRandomString(7);
    		$v->is_used = 0;
    		$v->created_at = date('Y-m-d H:i:s');
    		$v->updated_at = date('Y-m-d H:i:s');
    		$v->save();
    	}
    	return back();
    }
}
