<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use App\Category;
use App\Items;
use App\Shops;
use App\Orders;
use App\DeliveryMan;
use App\Voucher;


class DashboardController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $data['total_category'] = Category::count();
        $data['total_shops'] = Shops::count();
        $data['total_items'] = Items::count();
        $data['total_voucher'] = Voucher::count();
        $data['total_order'] = Orders::count();
        $data['total_deliveryman'] = DeliveryMan::count();
    	return view('backend.dashboard', $data);
    }

    public function sampleForm() {
    	return view('backend.sample-form');
    }
}
