<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Items;
use App\ItemImages;
use App\Category;
use App\Shops;
use Validator;

class ItemController extends Controller
{

    public function index() {
    	$items = Items::select(['items.id', 
            'items.name', 'items.price', 'items.is_active', 
            'items.description', 'items.tag', 'items.created_at',
            \DB::raw('shops.name as shop_name'),
            \DB::raw('category.name as category_name')
        ])
        ->leftJoin('category', 'category.id', '=', 'items.category_id')
        ->leftJoin('shops', 'shops.id', '=', 'items.shop_id')
        ->where('items.is_active', 1)
		->orderBy('items.id', 'desc')    	
		->get();
    	
    	return view('backend.items.list')
    	->with('table_name', 'Items')
    	->with('items', $items);
    }


    public function create() {

        $categories = Category::where('is_active', 1)->get();
        $shops = Shops::where('is_active', 1)->get();

    	return view('backend.items.create')
    	->with('table_name', 'Item Create')
        ->with('categories', $categories)
        ->with('shops', $shops);	
    }


    public function createAction(Request $r) {
        $validator = Validator::make($r->all() ,[
          'category_id' => 'required|integer',
          'shop_id' => 'required|integer',
          'name' => 'required|unique:shops',          
          'price' => 'required|numeric',                    
          'item_image' => 'required',
          'imte_image.*' => 'mimes:jpeg,jpg,png',
          'tag' => 'required'
        ]);

        // dd($r->all());
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($r->all());
        }

        $date = date('Y-m-d H:i:s');

        $item = new Items();
        $item->category_id = $r->category_id;
        $item->shop_id = $r->shop_id;
        $item->name = $r->name;
        $item->price = $r->price;
        $item->is_active = 1;
        $item->tag = $r->tag;
        $item->created_at = $date;
        $item->updated_at = $date;
        $item->save();


        foreach($r->item_image as $key => $p) {
            // print_r($p);
            
            $fileExt = $p->extension();
            // pathinfo($_FILES['item_image']['name'], PATHINFO_EXTENSION);
            $filename = time()."_ITEM_".date('Y-m-d_H_i_s'). ".". $key . $fileExt;
            $p->move(public_path('item/'), $filename); 

            // echo $filename. "<br/>";
                

            $itemImage = new ItemImages();
            $itemImage->item_id = $item->id;
            $itemImage->image = $filename;
            $itemImage->type = 0;
            $itemImage->created_at = $date;
            $itemImage->updated_at = $date;
            $itemImage->save();
        }

        Toastr::success('Item created successfully :)','Success');

        return back()->with('success', 'Data saved successfully');
    }


    public function delete() {
        return view('backend.items.delete');
    }

    public function destroy(Request $r) {
        $id = $r->id;
        if (!empty($id)) {
            $i = Items::find($id);
            $i->delete();
            Toastr::success('Item deleted successfully :)','Success');
        }
        return redirect()->back();
    }






}
