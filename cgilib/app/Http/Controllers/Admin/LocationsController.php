<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use App\Locations;
use Validator;


class LocationsController extends Controller
{
    public function index() {
    	$locations = Locations::orderBy('id', 'desc')->get();
    	return view('backend.locations.list')
        ->with('locations', $locations)
        ->with('table_name', 'Locations');
    }

    public function create() {
    	return view('backend.locations.create')
        ->with('table_name', 'Location Create');
    }



	public function createAction(Request $r) {
		
        //dd($r->all());       
        $validator = Validator::make($r->all() ,[
          'name' => 'required|unique:locations'          
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($r->all());
        }
      
		$l = new Locations();
		$l->name = $r->name;
		$l->is_active = true;
		$l->created_at = date('Y-m-d H:i:s');
		$l->save();

        Toastr::success('Location created successfully :)','Success');
		return back()->with('success', 'Locations added!');
	}


}
