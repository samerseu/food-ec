<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Shops;
use App\Category;
use App\Items;
use App\ItemImages;
use App\Locations;


class HomeController extends Controller
{
    public function home() {

       $items = Items::select(['items.id', 
            'items.name', 'items.price', 'items.is_active', 
            'items.description', 'items.tag', 'items.created_at',
            \DB::raw('shops.name as shop_name'),
            \DB::raw('shops.id as shop_id'),
            \DB::raw('shops.logo as shop_logo'),
            \DB::raw('shops.location_id as shop_location_id'),
            \DB::raw('category.name as category_name')
        ])
        ->leftJoin('category', 'category.id', '=', 'items.category_id')
        ->leftJoin('shops', 'shops.id', '=', 'items.shop_id')
        ->where('items.is_active', 1)
        ->orderBy('items.id', 'desc')       
        ->get();

        $itemImages = ItemImages::get();


        $locations = Locations::get();

        $data['items'] = $items;
        $data['itemImages'] = $itemImages;
        $data['locations'] = $locations;

        $data['shops'] = Shops::where('is_active', 1)->get();

        $data['categories'] = Category::
        where('is_active', 1)
        ->orderBy('id', 'desc')
        ->take(4)->get();

        // dd($data);
    	return view('site.home', $data);
    }
   
    public function foodDetail(Request $r) {
        // dd($r->all());
    	return view('site.food-detail');
    }
	
	public function FestaurantDetail(Request $r) {
        // dd($r->all());
    	return view('site.restaurant-detail');
    }

    public function restuarantDetail($id) {
        // echo $id; exit();
        $shop = Shops::where('id', $id)->first();
        
        $items = Items::where('shop_id', $id)->get();
        $itemImages = Items::select(['item_images.id', 'item_images.image', 'item_images.item_id'])
        ->leftJoin('item_images', 'item_images.item_id', '=', 'items.id')
        ->where('items.shop_id', $id)
        ->get();        

        $categories = Category::get();


        // foreach ($categories as $category) {
        //     echo $category->name . '<br/>';
        //     echo '====================<br/>';
        //     foreach($items as $item) {
        //         if ($item->category_id == $category->id) {
        //             echo $item->name . "<br/>";
        //         }                
        //     }   
        //     echo '====================<br/>';
        // }


        // dd($items, $itemImages);
        // exit();




        return view('site.restaurant-detail')
        ->with('categories', $categories)
        ->with('shop', $shop)
        ->with('items', $items)
        ->with('itemImages', $itemImages);
    }

}
