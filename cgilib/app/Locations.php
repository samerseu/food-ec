<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table = 'locations';
    protected $fillable = [
    	'name',
    	'is_active',
    	'created_at',
    	'updated_at'
    ];
}
