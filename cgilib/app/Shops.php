<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{
    protected $table = 'shops';
    protected $fillable = [
    	'name',
    	'address',
    	'mobile',
    	'website',
    	'email',
    	'location_id',
    	'is_active',
    	'created_at',
    	'updated_at'
    ];
}
